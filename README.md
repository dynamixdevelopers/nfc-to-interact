# Welcome
This README provides an overview of the `org.ambientdynamix.contextplugins.nfctointeract` plug-in for the [Dynamix Framework](http://ambientdynamix.org). It allows users to scan a NFC Tag and retrieve the associated data. 

Note that this guide assumes an understanding of the [Dynamix Framework documentation](http://ambientdynamix.org/documentation/).

# Plug-in Requirements
* Dynamix version 2.1 and higher

# Native App Usage 
Integrate the Dynamix Framework within your app as [shown here](http://ambientdynamix.org/documentation/integration/). This guide demonstrates how to connect to Dynamix, create context handlers, register for context support, receive events, interact with plug-ins, and use callbacks. To view a simple example of using Dynamix in your native programs, see [this guide](http://ambientdynamix.org/documentation/native-app-quickstart).

## Context Support
* `org.ambientdynamix.contextplugins.nfctointeract.scan` initiate an NFC tag scan. 

## Handle Events
Once context support is successfully added, apps can make requests to initiate scanning. On a successful scan, the plugin responds with a context event of the type `NFCInfo`, which contains the following data :

1. `id` : id of the scanned Tag as a String.
2. `payload` : data scanned from the NFC Tag as a String.

Apps can make use of the `getId()` and `getPayload()` methods of the NFCInfo class to retrieve these values. 

 






