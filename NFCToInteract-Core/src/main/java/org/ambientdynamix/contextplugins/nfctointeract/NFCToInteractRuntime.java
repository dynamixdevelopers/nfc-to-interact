/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.nfctointeract;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.ContextListenerInformation;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PowerScheme;

import java.util.UUID;

/**
 * Example auto-reactive plug-in that detects the device's battery level.
 *
 * @author Darren Carlson
 */
public class NFCToInteractRuntime extends ContextPluginRuntime {
    // Static logging TAG
    private final String TAG = this.getClass().getSimpleName();
    // Our secure context
    private Context context;


    /**
     * Called once when the ContextPluginRuntime is first initialized. The implementing subclass should acquire the
     * resources necessary to run. If initialization is unsuccessful, the plug-ins should throw an exception and release
     * any acquired resources.
     */
    @Override
    public void init(PowerScheme powerScheme, ContextPluginSettings settings) throws Exception {
        // Set the power scheme
        this.setPowerScheme(powerScheme);
        // Store our secure context
        this.context = this.getSecuredContext();
    }

    /**
     * Called by the Dynamix Context Manager to start (or prepare to start) context sensing or acting operations.
     */
    @Override
    public void start() {
        // Register for battery level changed notifications
        Log.d(TAG, "Started!");
    }

    /**
     * Called by the Dynamix Context Manager to stop context sensing or acting operations; however, any acquired
     * resources should be maintained, since start may be called again.
     */
    @Override
    public void stop() {
        // Unregister battery level changed notifications

    }

    /**
     * Stops the runtime (if necessary) and then releases all acquired resources in preparation for garbage collection.
     * Once this method has been called, it may not be re-started and will be reclaimed by garbage collection sometime
     * in the indefinite future.
     */
    @Override
    public void destroy() {

    }

    @Override
    public void handleContextRequest(UUID requestId, String contextType) {
        if (contextType.equals(NFCInfo.CONTEXT_TYPE)) {
            NfcToInteractFeature(requestId);
        } else {
            sendContextRequestError(requestId, "Context Type not supported", ErrorCodes.CONTEXT_TYPE_NOT_SUPPORTED);
        }
    }

    @Override
    public void handleConfiguredContextRequest(UUID requestId, String contextType, Bundle config) {

    }

    @Override
    public void updateSettings(ContextPluginSettings settings) {
    }

    @Override
    public void setPowerScheme(PowerScheme scheme) {
    }


    @Override
    public boolean addContextlistener(ContextListenerInformation listenerInfo) {
        return true;
    }

    public void NfcToInteractFeature(UUID requestId) {
        openView(new NFCToInteract(getSecuredContext(), this, requestId));
    }

}