package org.ambientdynamix.contextplugins.nfctointeract;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NfcA;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import org.ambientdynamix.api.contextplugin.ActivityController;
import org.ambientdynamix.api.contextplugin.ContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.IPluginView;
import org.ambientdynamix.api.contextplugin.PluginConstants;

import java.util.UUID;

/**
 * Created by shivam on 4/15/15.
 */
public class NFCToInteract implements IPluginView {

    private ActivityController controller;
    Context context;
    Tag detectedTag;
    private ContextPluginRuntime runtime;
    private UUID requestId;
    private final String NFC_SCAN_HINT = "Tap to Scan NFC Tags";


    public NFCToInteract(Context context, ContextPluginRuntime runtime, UUID requestId) {
        this.context = context;
        this.runtime = runtime;
        this.requestId = requestId;
    }

    @Override
    public void setActivityController(ActivityController activityController) {
        controller = activityController;
    }


    private String getIdFrombytes(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02X", b));
        }
        return sb.toString();
    }

    @Override
    public View getView() {
        LinearLayout parentLayout = new LinearLayout(context);
        parentLayout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        parentLayout.setOrientation(LinearLayout.VERTICAL);
        parentLayout.setGravity(Gravity.CENTER);
        TextView hintTextView = new TextView(context);
        hintTextView.setText(NFC_SCAN_HINT);
        hintTextView.setTextSize(18);//24sp
        hintTextView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        hintTextView.setGravity(Gravity.CENTER);
        parentLayout.addView(hintTextView);
        return parentLayout;
    }

    @Override
    public void destroyView() {
        controller.closeActivity();

    }

    @Override
    public void handleIntent(Intent intent) {
        Log.d(getClass().getSimpleName(), "New Intent Received : " + intent.getAction());
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action) || NfcAdapter.ACTION_TAG_DISCOVERED.equals(action) || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            detectedTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String[] techList = detectedTag.getTechList();
            NfcA tagNfcA = NfcA.get(detectedTag);
            String id = getIdFrombytes(detectedTag.getId());
            Log.i(getClass().getSimpleName(), "sensed TAG with ID: " + id);
            readFromTag(id, intent);

        }
    }

    public void readFromTag(String id, Intent intent) {

        Ndef ndef = Ndef.get(detectedTag);
        try {
            ndef.connect();
            Parcelable[] messages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

            if (messages != null) {
                NdefMessage[] ndefMessages = new NdefMessage[messages.length];
                for (int i = 0; i < messages.length; i++) {
                    ndefMessages[i] = (NdefMessage) messages[i];
                }
                NdefRecord record = ndefMessages[0].getRecords()[0];

                byte[] payload = record.getPayload();
                String text = new String(payload);
                ndef.close();
                runtime.sendContextEvent(requestId, new NFCInfo(id, text));
                controller.closeActivity();
            }
        } catch (Exception e) {
            Bundle alert = new Bundle();
            alert.putString(PluginConstants.ALERT_TYPE, PluginConstants.ALERT_TYPE_TOAST);
            alert.putString(PluginConstants.ALERT_TOAST_VALUE, e.getMessage());
            alert.putInt(PluginConstants.ALERT_TOAST_DURATION, Toast.LENGTH_LONG);
            runtime.sendPluginAlert(alert);
            e.printStackTrace();
        }
    }

    @Override
    public int getPreferredOrientation() {
        return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    }
}
